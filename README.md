

# Transformation du thésaurus de Datatourisme en SKOS

Named graph : https://www.datatourisme.gouv.fr/resource/thesaurus#NG


Lien vers la doc :
* [Datatourisme Thésau](https://fdymnx.gitlab.io/skos-experiment/voc/datatourisme_thesau/)
* [Datatourisme POI Thésau](https://fdymnx.gitlab.io/skos-experiment/voc/datatourisme_POI-thesau/)

Prérequis :
* avoir chargé toutes les données de l'ontologie Datatourisme depuis https://framagit.org/datatourisme/ontology/-/tree/master/, i.e ontologie datatourisme.ttl + thesaurus.ttl

Principe :
Dans les données du thesaurus, on part des owl:NamedIndividual qui correspondent aux concepts "feuilles" et qui sont typés avec une classe qui elle même est sous-classe d'une autre classe. On récrée donc une hiérarchie à 3 niveaux en considérant la classe la plus haute comme top concept, suivi de sa sous-classe, et enfin des "namedIndividuals".

Requête SPARQL :
```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <https://www.datatourisme.gouv.fr/ontology/core#>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> 
prefix dtres: <https://www.datatourisme.gouv.fr/resource/core#> 

# SELECT *
CONSTRUCT {
    <https://www.datatourisme.gouv.fr/resource/thesaurus#voc> a mnx:Vocabulary; 
  skos:prefLabel "Thesaurus Datatourisme (by Mmnemotix)" .

    <https://www.datatourisme.gouv.fr/resource/thesaurus> a skos:ConceptScheme ;
                                                           mnx:schemeOf <https://www.datatourisme.gouv.fr/resource/thesaurus#voc> ;
                                                           skos:prefLabel "Main branch - Datatourisme"@en, "Branche Principale"@fr ;
                                                          skos:hasTopConcept ?topConcept .
    ?topConcept a skos:Concept ;
                skos:definition ?topConceptCom ;
                skos:prefLabel ?topConceptLabel .
    ?leafType a skos:Concept ;
              skos:definition ?leafTypeCom ;
              skos:prefLabel ?leafTypeLabel .
    ?leafType skos:broader ?topConcept .
    ?indiv a skos:Concept ;
           skos:prefLabel ?indivLabel ;
    	    skos:broader ?leafType .	
}
#SELECT DISTINCT ?schemeType
WHERE {
    ?indiv a owl:NamedIndividual;
           a ?leafType ;
           rdfs:label ?indivLabel .
    ?leafType rdfs:label ?leafTypeLabel . 
    FILTER((?leafType != <https://www.datatourisme.gouv.fr/ontology/core#City> ))
    
    OPTIONAL{?leafType rdfs:comment ?leafTypeCom ;}
    OPTIONAL{
        ?leafType rdfs:subClassOf ?topConcept . 
        ?topConcept rdfs:label ?topConceptLabel .
        FILTER(!CONTAINS(STR(?topConcept), "schema.org"))
        OPTIONAL{ ?topConcept rdfs:comment ?topConceptCom}
    	}  
}
```

2ème requête pour le thesaurus des types de POI

```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <https://www.datatourisme.gouv.fr/ontology/core#>
prefix mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> 

#select ?topConcept ?concept1 ?concept2 ?concept3 ?concept4
CONSTRUCT {

        <https://www.datatourisme.gouv.fr/resource/thesaurus#voc> a mnx:Vocabulary; 
  skos:prefLabel "Thesaurus Datatourisme (by Mmnemotix)" .

    <https://www.datatourisme.gouv.fr/resource/POIthesaurus> a skos:ConceptScheme ;
                                                           mnx:schemeOf <https://www.datatourisme.gouv.fr/resource/thesaurus#voc> ;
                                                           skos:prefLabel "POI - Points Of Interest"@en, "POI - Points d'intérêt"@fr ;
                                                          skos:hasTopConcept ?topConcept .
    
    ?topConcept a skos:Concept ;
                skos:prefLabel ?topCl.
    ?concept1 a skos:Concept;
              skos:broader ?topConcept ;
                skos:prefLabel ?c1l .
    ?concept2 a skos:Concept;
              skos:broader ?concept1 ;
                skos:prefLabel ?c2l .
    ?concept3 a skos:Concept;
              skos:broader ?concept2 ;
                skos:prefLabel ?c3l  .
    ?concept4 a skos:Concept;
              skos:broader ?concept3 ;
                skos:prefLabel ?c4l  .
}
where { 
	?topConcept rdfs:subClassOf <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> ;
            rdfs:label ?topCl.
    ?concept1 rdfs:subClassOf ?topConcept  ;
              rdfs:label ?c1l.
    OPTIONAL{
        ?concept2 rdfs:subClassOf ?concept1 ;
              rdfs:label ?c2l.
        OPTIONAL{
        	?concept3 rdfs:subClassOf ?concept2 ;
              rdfs:label ?c3l.
            OPTIONAL {
              	?concept4 rdfs:subClassOf ?concept3 ;
	            rdfs:label ?c4l.
            }
        }
    }
    
} 

```

# règle d'inférence avec ConceptScheme 
Tests d'une règle d'inférence graphdb pour inférer qu'un concept pris dans une hierarchie appartient au même ConceptScheme que le concept en bout de chaine

## Ajout d'une règle sur la base de RDFS-plus-opti

cf [rdfsplus-opti-inscheme.pie](rdfsplus-opti-inscheme.pie)

On ajoute juste les bons préfixes au début et à la fin :

```
  Id: inScheme_Transitive_Inference 

      a <skos:broaderTransitive> b 
      b <skos:topConceptOf> s 
    ------------------------------------
      a <skos:inScheme> s 
```

## Données de test

```ttl
@prefix data: <http://test-data.org#> .

data:S1 a skos:ConceptScheme ;
    skos:hasTopConcept data:C1 .

data:C1 a skos:Concept ;
    skos:inScheme data:S1 ;
    skos:topConceptOf data:S1 .

data:C2 a skos:Concept ;
    skos:broader data:C1 .

data:C3 a skos:Concept ;
    skos:broader data:C2 .

```

## Requête 

Une fois que les données + l'ontologie SKOS sont chargées, on a bien le résultat escompté, à savoir que la requête ci-après renvoie bien les 3 concepts C1, C2, et C3

```sparql
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX data: <http://test-data.org#>

select * where { 
	?c skos:inScheme data:S1
} 
```